from django.db import models
from time import time

# Create your models here.
def get_upload_file_name(instance, filename):
    return "uploaded_files/%s_%s" % (str(time()).replace('.','_'), filename)

class UploadImage(models.Model):
    thumbnail = models.FileField(upload_to=get_upload_file_name)
    def __unicode__(self):
        return self.thumbnail

class Booking(models.Model):
    name = models.CharField(("Name"), max_length=50)
    email = models.EmailField(("Email"), max_length=100)
    phone_number = models.CharField("Phone Number", max_length=15)
    message = models.CharField("Message", max_length=200)