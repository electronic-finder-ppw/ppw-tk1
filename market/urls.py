from django.urls import path

from . import views

app_name = "market"

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('booking/', views.booking, name='booking'),
    path('booking/add-booking/',views.AddBooking, name='add booking')
]
