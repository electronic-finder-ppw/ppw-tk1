from django import forms
from .models import UploadImage

class Search_image(forms.ModelForm):
    class Meta:
        model = UploadImage
        fields = ('thumbnail',)

class BookingForm(forms.Form):
    name_attrs = {'type':'text', 'class':'form-group', 'placeholder':'Enter Your Name'}
    email_attrs = {'type':'email','class':'form-group', 'placeholder':'something@host.com'}
    phone_number_attrs = {'type':'text', 'class':'form-group', 'placeholder':'Enter Your Phone Number'}
    message_attrs = {'type':'text', 'class':'form-group', 'placeholder':'Enter Your Message For Us!'}

    name = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(label='', required=True, widget=forms.EmailInput(attrs=email_attrs))
    phone_number = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=phone_number_attrs))
    message = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=message_attrs))
