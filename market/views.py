from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import Search_image, BookingForm
from .models import Booking
from random import sample

def homepage(request):
	response = {"Search_image":Search_image}
	return render(request, "Homepage.html", response)

def booking(request):
	#TODO: get objectID from request.POST
	response = {"Search_image":Search_image, 'Booking':BookingForm}
	return render(request, "Booking.html", response)

def AddBooking(request):
	form = BookingForm(request.POST or None)
	
	if(request.method == 'POST' and form.is_valid()):
		name = form.cleaned_data['name']
		email = form.cleaned_data['email']
		phone_number = form.cleaned_data['phone_number']
		message = form.cleaned_data['message']

		booking = Booking(name=name, email=email, phone_number=phone_number, message=message)
		# booking.save()

	return HttpResponseRedirect('/')


