from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import Newsletter
from .models import Store, Newsletter_Member
from search_result.models import Electronic
from random import sample
from search_result.models import Electronic

def storepage(request):

    try:
        if (request.method == 'POST'):
            item_id = request.POST.get("item_id")
            item_name = Electronic.objects.get(item_id=item_id).name
        else:
            raise Exception
    except:
        item_id = 1
        item_name = "PLACEHOLDER"

    response = {"item_name":item_name, "item_id":item_id, "newsletter":Newsletter}

    count = Store.objects.all().count()
    
    stores = []
    try:
        randomized = sample(range(1, count+1), 3)
        for number in randomized:
            stores.append(Store.objects.get(store_id=number))
    except:
        pass
    response["stores"] = stores

    return render(request, "storepage.html", response)

def subscribe(request):
    form = Newsletter(request.POST or None)

    if(request.method == 'POST' and form.is_valid()):
        email = form.cleaned_data['email']

        member = Newsletter_Member(email=email)
        member.save()
    
    return HttpResponseRedirect('/storepage/')


