from django.urls import path

from . import views

urlpatterns = [
    path('', views.storepage, name='storepage'),
    path('subscribe/', views.subscribe, name='subscribe')
]