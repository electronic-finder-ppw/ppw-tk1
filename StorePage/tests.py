from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import storepage, subscribe
from .models import Store, Newsletter_Member
from django.http import HttpRequest
import unittest
from io import BytesIO
from PIL import Image
from django.core.files.base import File

def get_image_file(name='test.png', ext='png', size=(50, 50), color=(256, 0, 0)):
    file_obj = BytesIO()
    image = Image.new("RGBA", size=size, color=color)
    image.save(file_obj, ext)
    file_obj.seek(0)
    return File(file_obj, name=name)

class StorePageUnitTest(TestCase):

    def test_storepage_exist(self):
        response = Client().get('/storepage/')
        self.assertEqual(response.status_code,200)
        response = Client().post('/storepage/',{'item_id':1})

    def test_subscribe_redirects(self):
        response = Client().get('/storepage/subscribe/')
        self.assertEqual(response.status_code,302)

    def test_using_storepage_func(self):
        found = resolve('/storepage/')
        self.assertEqual(found.func, storepage)
        
    def test_using_subscribe_func(self):
        found = resolve('/storepage/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_storepage_contains_text(self):
        request = HttpRequest()
        response = storepage(request)
        html_response = response.content.decode('utf8')
        self.assertRegex(html_response, r'<div class="h1 text-center">You can buy')
    
    def test_storepage_contains_subscribe(self):
        request = HttpRequest()
        response = storepage(request)
        html_response = response.content.decode('utf8')
        self.assertRegex(html_response, r'<font class="h3">Submit')

    def test_models_works(self):
        mock_store = Store()
        mock_store.store_id = 1
        mock_store.picture = get_image_file()
        mock_store.name = "mock_store"
        mock_store.address_first = "mock_store_address1"
        mock_store.address_second = "mock_store_address2"
        mock_store.details = "mock_store_details"
        mock_store.save()

        self.assertEqual(Store.objects.count(), 1)
        self.assertIsInstance(mock_store, Store)

        mock_member = Newsletter_Member()
        mock_member.email = "Something@gmail.com"
        mock_member.save()

        self.assertEqual(Newsletter_Member.objects.count(), 1)
        self.assertIsInstance(mock_member, Newsletter_Member)
    
    def test_views_models_randomize_works(self):
        mock_store1 = Store()
        mock_store1.store_id = 1
        mock_store1.picture = get_image_file()
        mock_store1.name = "mock_store1"
        mock_store1.address_first = "mock_store1_address1"
        mock_store1.address_second = "mock_store1_address2"
        mock_store1.details = "mock_store1_details"
        mock_store1.save()

        mock_store2 = Store()
        mock_store2.store_id = 2
        mock_store2.picture = get_image_file()
        mock_store2.name = "mock_store2"
        mock_store2.address_first = "mock_store2_address1"
        mock_store2.address_second = "mock_store2_address2"
        mock_store2.details = "mock_store2_details"
        mock_store2.save()

        mock_store3 = Store()
        mock_store3.store_id = 3
        mock_store3.picture = get_image_file()
        mock_store3.name = "mock_store3"
        mock_store3.address_first = "mock_store3_address1"
        mock_store3.address_second = "mock_store3_address2"
        mock_store3.details = "mock_store3_details"
        mock_store3.save()

        request = HttpRequest()
        response = storepage(request)
        html_response = response.content.decode('utf8')
        self.assertRegex(html_response, r'<p class="storepage_text h4">Nama : mock_store1')
        self.assertRegex(html_response, r'<p class="storepage_text h4">Nama : mock_store2')
        self.assertRegex(html_response, r'<p class="storepage_text h4">Nama : mock_store3')
    
    def test_newsletter_form_works(self):
        client = Client()
        response = client.post("/storepage/subscribe/", {'email':'nadhif.ap@gmail.com'})

        self.assertEqual(response.status_code,302)
        self.assertEqual(Newsletter_Member.objects.count(), 1)

