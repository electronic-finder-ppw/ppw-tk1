from django.contrib import admin

from .models import Newsletter_Member, Store

admin.site.register(Store)
admin.site.register(Newsletter_Member)
