from django import forms

class Newsletter(forms.Form):

        email_attrs = {'type':'email','class':'form-group', 'placeholder':'something@host.com'}
        
        email = forms.EmailField(label='', required=True, widget=forms.EmailInput(attrs=email_attrs))