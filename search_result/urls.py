from django.urls import path
from . import views

urlpatterns = [
    path('', views.searchpage, name='searchpage'),
    path('subscribe/', views.subscribe, name='subscribe')
]