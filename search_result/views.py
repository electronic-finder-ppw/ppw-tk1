from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import Newsletter
from .models import Electronic, Subscribtion
from random import sample
import random
# Create your views here.
def searchpage(request):
	size = Electronic.objects.all().count()
	randomized = random.randint(1,size)
    # randomized = sample(range(1, size+1), 1)
	produk = Electronic.objects.get(item_id = randomized)
	context = {
    	'item' : produk
    }
	return render(request, "searchresult.html", context)

def subscribe(request):
    form = Newsletter(request.POST or None)

    if(request.method == 'POST' and form.is_valid()):
        email = form.cleaned_data['email']

        member = Subscribtion(email=email)
        member.save()
    
    return HttpResponseRedirect('/searchresult/')