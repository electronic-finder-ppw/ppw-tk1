from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import searchpage, subscribe
from .models import Electronic, Subscribtion
from django.http import HttpRequest
import unittest
from io import BytesIO
from PIL import Image
from django.core.files.base import File

def get_image_file(name='test.png', ext='png', size=(50, 50), color=(256, 0, 0)):
    file_obj = BytesIO()
    image = Image.new("RGBA", size=size, color=color)
    image.save(file_obj, ext)
    file_obj.seek(0)
    return File(file_obj, name=name)

class SearchPageTest(TestCase):

	def subscribtion_test(self):
		response = Client().get('/searchresult/subscribe/')
		self.assertEqual(response.status_code,302)

	def model_test(self):
		pure = Electronic()
		pure.item_id = 1
		pure.brand = "sumsang"
		pure.picture = get_image_file()
		pure.name = "10S"
		pure.processor = "amd"
		pure.display = "tn panel"
		pure.graphic = "amd gpu"
		pure.memory = "90gb"
		pure.camera = "12mp"
		pure.battery = "4000 mah"
		pure.save()

		self.assertEqual(Electronic.objects.count(), 1)
		self.assertIsInstance(pure, Electronic)
		subs = Newsletter_Member()
		subs.email = "Something@gmail.com"
		subs.save()

		self.assertEqual(Newsletter_Member.objects.count(), 1)
		self.assertIsInstance(subs, Newsletter_Member)